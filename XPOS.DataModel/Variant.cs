﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace XPOS.DataModel
{
    public class Variant : BaseTable
    {
        [Key, DatabaseGenerated
            (DatabaseGeneratedOption.Identity)]
        public long CategoryId { get; set; }

        [Required, StringLength(50)]
        public string Initial { get; set; }
        public string Name { get; set; }
        public bool Active { get; set; }
        public virtual ICollection<Variant> Category { get; set; }
    }
}
